package com.example.login

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.kegiatan_list.view.*

class RVAdapterKegiatan(private val context: Context, private val arrayList: ArrayList<Kegiatans>) : RecyclerView.Adapter<RVAdapterKegiatan.Holder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        return Holder(LayoutInflater.from(parent.context).inflate(R.layout.kegiatan_list,parent,false))
    }

    override fun getItemCount(): Int = arrayList!!.size

    override fun onBindViewHolder(holder: Holder, position: Int) {

        holder.view.lbIdList.text= arrayList?.get(position)?.kegiatan_id
        holder.view.lbBPHList.text = "BPH : "+arrayList?.get(position)?.bph_id
        holder.view.lbPeriodeList.text = "Periode : "+arrayList?.get(position)?.periode_id
        holder.view.lbNamaList.text = "Nama Kegiatan : "+arrayList?.get(position)?.nama_kegiatan
        holder.view.lbWaktuList.text = "Waktu Kegiatan : "+arrayList?.get(position)?.waktu_kegiatan
        holder.view.lbTempatList.text = "Tempat_Kegiatan : "+arrayList?.get(position)?.tempat_kegiatan
        holder.view.lbDeskripsiList.text = "Deskripsi_Kegiatan : "+arrayList?.get(position)?.deskripsi_kegiatan
        holder.view.lbBerakhirList.text = "Tanggal_berakhir : "+arrayList?.get(position)?.tanggal_berakhir
//        holder.view.lbImageList.text = "Image : "+arrayList?.get(position)?.foto_kegiatan
        holder.view.lbStatusList.text = "Status_kegiatan : "+arrayList?.get(position)?.status_kegiatan

    }

    class Holder(var view: View) : RecyclerView.ViewHolder(view)
}