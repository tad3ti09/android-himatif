package com.example.login.Session

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import com.example.login.Home.HomeActivity
import com.example.login.LoginActivity
import com.example.login.Model.Mahasiswa

class SharedPrefManager {
    private var SHARED_NAME = "shared"
    private var mahasiswa_id ="mahasiswa_id"
    private var nama = "nama"
    private var username = "username"
    private var password = "password"
    private var nim = "nim"
    private var kelas = "kelas"
    private var angkatan = "angkatan"
    private var alamat = "alamat"
    private var email = "email"
    private var status_mahasiswa =  "status_mahasiswa"
    private var tempat_lahir = "tempat_lahir"
    private var tanggal_lahir = "tanggal_lahir"
    private var role = "role"
    private var foto_mahasiswa = "foto_mahasiswa"


    var mInstance: SharedPrefManager?=null
    lateinit var mCtx : Context
    lateinit var sharedPreferences: SharedPreferences
    lateinit var editor: SharedPreferences.Editor



    constructor(context: Context?){
        if (context != null) {
            mCtx = context
        }
    }

    @Synchronized fun getInstance(context: Context?):SharedPrefManager{
        if (mInstance == null){
            mInstance =SharedPrefManager(context)
        }
        return mInstance as SharedPrefManager
    }

    fun isLoogedin():Boolean{
        var share:SharedPreferences
        share = mCtx.getSharedPreferences(SHARED_NAME, Context.MODE_PRIVATE)
        return share.getString(email, null)!=null
    }

    fun mahasiswaLogin(mahasiswaModel:Mahasiswa){
        sharedPreferences =mCtx.getSharedPreferences(SHARED_NAME, Context.MODE_PRIVATE)
        editor = sharedPreferences.edit()
        editor.putString(mahasiswa_id, mahasiswaModel.mahasiswa_id)
        editor.putString(nama, mahasiswaModel.nama)
        editor.putString(username, mahasiswaModel.username)
        editor.putString(password, mahasiswaModel.password)
        editor.putString(nim, mahasiswaModel.nim)
        editor.putString(kelas, mahasiswaModel.kelas)
        editor.putString(angkatan, mahasiswaModel.angkatan)
        editor.putString(alamat, mahasiswaModel.alamat)
        editor.putString(email, mahasiswaModel.email)
        editor.putString(status_mahasiswa, mahasiswaModel.status_mahasiswa)
        editor.putString(tempat_lahir, mahasiswaModel.tempat_lahir)
        editor.putString(tanggal_lahir, mahasiswaModel.tanggal_lahir)
        editor.putString(role, mahasiswaModel.role)
        editor.putString(foto_mahasiswa, mahasiswaModel.foto_mahasiswa)
        editor.apply()

    }

    fun getMahasiswa():Mahasiswa{
        var shared :SharedPreferences
        shared = mCtx.getSharedPreferences(SHARED_NAME, Context.MODE_PRIVATE)
        return Mahasiswa(
                shared.getString(mahasiswa_id, null),
                shared.getString(nama, null),
                shared.getString(username, null),
                shared.getString(password, null),
                shared.getString(nim, null),
                shared.getString(kelas, null),
                shared.getString(angkatan, null),
                shared.getString(alamat, null),
                shared.getString(email, null),
                shared.getString(status_mahasiswa, null),
                shared.getString(tempat_lahir, null),
                shared.getString(tanggal_lahir, null),
                shared.getString(role, null),
                shared.getString(foto_mahasiswa, null)
                )
    }

    fun logout(){
        var shared:SharedPreferences = mCtx.getSharedPreferences(SHARED_NAME, Context.MODE_PRIVATE)
        editor =shared.edit()
        editor.clear()
        editor.apply()
        mCtx.startActivity(Intent(mCtx, LoginActivity::class.java))
    }

    fun checkLogin(){
        if(!this.isLoogedin()){
        var i =Intent(mCtx, LoginActivity::class.java)
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            mCtx.startActivity(i)
        }

        else{
            var i = Intent(mCtx, HomeActivity::class.java)
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            mCtx.startActivity(i)
        }
    }

}