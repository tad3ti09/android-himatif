package com.example.login

import android.app.ProgressDialog
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.widget.Toast
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import com.example.login.Api.ApiEndPoint
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONObject

class MainActivity : AppCompatActivity() {

    var arrayList= ArrayList<Kegiatans>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        supportActionBar?.title = "Data Kegiatan"

        mRecyclerView.setHasFixedSize(true)
        mRecyclerView.layoutManager = LinearLayoutManager(this);


    }
    override fun onResume(){
        super.onResume()
//        loadAllStudents()
    }
//    private fun loadAllStudents(){
//        val loading = ProgressDialog(this)
//        loading.setMessage("Memuat data...")
//        loading.show()
//        AndroidNetworking.get(ApiEndPoint.READ)
//                .setPriority(Priority.MEDIUM)
//                .build()
//                .getAsJSONObject(object : JSONObjectRequestListener {
//                    override fun onResponse(response: JSONObject?) {
//                        arrayList.clear()
//                        val jsonArray = response?.optJSONArray("result")
//                        if (jsonArray?.length()== 0)
//                        {
//                            loading.dismiss()
//                            Toast.makeText(applicationContext,"Kegiatan data is empty, Add the data first",
//                                    Toast.LENGTH_SHORT).show()
//                        }
//                        for(i in 0 until jsonArray?.length()!!){
//
//                            val jsonObject = jsonArray?.optJSONObject(i)
//                            arrayList.add(Kegiatans(jsonObject.getString("kegiatan_id"),
//                                    jsonObject.getString("bph_id"),
//                                    jsonObject.getString("periode_id"),
//                                    jsonObject.getString("nama_kegiatan"),
//                                    jsonObject.getString("waktu_kegiatan"),
//                                    jsonObject.getString("tempat_kegiatan"),
//                                    jsonObject.getString("deskripsi_kegiatan"),
//                                    jsonObject.getString("foto_kegiatan"),
//                                    jsonObject.getString("tanggal_berakhir"),
//                                    jsonObject.getString("status_kegiatan")))
//
//                            if(jsonArray?.length() - 1 == i){
//
//                                loading.dismiss()
//                                val adapter = RVAdapterKegiatan(applicationContext,arrayList)
//                                adapter.notifyDataSetChanged()
//                                mRecyclerView.adapter = adapter
//
//                            }
//
//                        }
//
//                    }
//
//                    override fun onError(anError: ANError?) {
//                        loading.dismiss()
//                        Log.d("ONERROR",anError?.errorDetail?.toString())
//                        Toast.makeText(applicationContext,"Connection Failure", Toast.LENGTH_SHORT).show()
//                    }
//
//                })
    }



