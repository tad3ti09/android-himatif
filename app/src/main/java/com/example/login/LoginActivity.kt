package com.example.login

import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.AppCompatButton
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.Toast
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import com.example.login.Api.ApiEndPoint
import com.example.login.Home.HomeActivity
import com.example.login.Model.Mahasiswa
import com.example.login.Session.SharedPrefManager
import org.json.JSONObject

class LoginActivity : AppCompatActivity() {
    //Defining views

    private var share: SharedPrefManager?=null
    private var editTextEmail: EditText? = null
    private var editTextPassword: EditText? = null
    private var context: Context? = null
    private var buttonLogin: AppCompatButton? = null
    private var pDialog: ProgressDialog? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        context = this@LoginActivity

        share = SharedPrefManager(this)
        //Initializing views
        pDialog = ProgressDialog(context)
        editTextEmail = findViewById<View>(R.id.editTextEmail) as EditText
        editTextPassword = findViewById<View>(R.id.editTextPassword) as EditText
        buttonLogin = findViewById<View>(R.id.buttonLogin) as AppCompatButton

        //Adding click listener
        buttonLogin!!.setOnClickListener {
            if(editTextEmail!!.text.toString().trim().equals("") || editTextPassword!!.text.toString().trim().equals("")){
                Toast.makeText(this@LoginActivity, "Form Must Be Filled", Toast.LENGTH_LONG).show()
            }
            else {
                loginUser(editTextEmail!!.text.toString().trim(), editTextPassword!!.text.toString().trim())
            }
        }
        //editTextEmail.setText("admin@agusharyanto.net");
        //editTextPassword.setText("abcd1234");
    }

    private fun loginUser(trim: String, trim1: String) {
        val loading = ProgressDialog(this)
        loading.setMessage("Loading Data....")
        loading.show()

        AndroidNetworking.post(ApiEndPoint.login)
                .addBodyParameter("email", trim)
                .addBodyParameter("password", trim1)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject?) {
                        var userJSON: JSONObject
                        var userModel:Mahasiswa
                        loading.dismiss()
                        if (response?.getString("message")?.contains("success")!!){
                            println("SUDAH SUKSES")
                            Toast.makeText(applicationContext, response?.getString("message"),Toast.LENGTH_LONG).show()
//                            userJSON = response.getJSONObject("DataUser")
//                            Log.d("Data User : ", response.getJSONObject("DataUser").getString("nama_depan").toString())

                            userModel = Mahasiswa(
                                    response.getJSONObject("DataUser").getString("mahasiswa_id"),
                                    response.getJSONObject("DataUser").getString("nama"),
                                    response.getJSONObject("DataUser").getString("username"),
                                    response.getJSONObject("DataUser").getString("password"),
                                    response.getJSONObject("DataUser").getString("nim"),
                                    response.getJSONObject("DataUser").getString("kelas"),
                                    response.getJSONObject("DataUser").getString("angkatan"),
                                    response.getJSONObject("DataUser").getString("alamat"),
                                    response.getJSONObject("DataUser").getString("email"),
                                    response.getJSONObject("DataUser").getString("status_mahasiswa"),
                                    response.getJSONObject("DataUser").getString("tempat_lahir"),
                                    response.getJSONObject("DataUser").getString("tanggal_lahir"),
                                    response.getJSONObject("DataUser").getString("role"),
                                    response.getJSONObject("DataUser").getString("foto_mahasiswa")
                            )
//
                            share!!.getInstance(applicationContext).mahasiswaLogin(userModel)
                            val i = Intent(this@LoginActivity, HomeActivity::class.java)
                            startActivity(i)
                            finish()
                        }
                        else{
                            println("BELUM SUKSES")
                            Toast.makeText(applicationContext, "Please To Register Your Account",Toast.LENGTH_LONG).show()
                            editTextEmail!!.setText("")
                            editTextPassword!!.setText("")
                        }
                    }

                    override fun onError(anError: ANError?) {
                        loading.dismiss()
                        println("ADA KESALAHANNYA")
                        Log.d("OneError : ",anError?.errorDetail?.toString())
                        Toast.makeText(this@LoginActivity,"Connection Failure", Toast.LENGTH_LONG).show()
                    }

                })
    }

}