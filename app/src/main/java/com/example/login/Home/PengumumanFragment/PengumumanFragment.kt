package com.example.login.Home.PengumumanFragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.login.Adapter.AdapterKegiatan
import com.example.login.Adapter.AdapterPengumuman
import com.example.login.Api.API
import com.example.login.Api.Common
import com.example.login.Model.Kegiatan
import com.example.login.Model.Pengumuman
import com.example.login.Model.ServerResponse
import com.example.login.Model.ServerResponsePengumuman

import com.example.login.R
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class PengumumanFragment : Fragment() {

    lateinit var dua:RecyclerView

    lateinit var mService: API
    lateinit var layoutManager: LinearLayoutManager
    lateinit var adapterPengumuman: AdapterPengumuman

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        var view= inflater.inflate(R.layout.fragment_pengumuman, container, false)
        dua = view.findViewById(R.id.dua)
        dua.setHasFixedSize(true)
        layoutManager = LinearLayoutManager(context)
        dua.layoutManager = layoutManager

        mService = Common.retrofitService

        getallpengumuman()
        return view
    }

    private fun getallpengumuman() {
        mService.getpengumuman().enqueue(object : Callback<ServerResponsePengumuman>{
            override fun onFailure(call: Call<ServerResponsePengumuman>, t: Throwable) {
                print("Error : "+t.message)
            }

            override fun onResponse(call: Call<ServerResponsePengumuman>, response: Response<ServerResponsePengumuman>) {
                val serverResponse: ServerResponsePengumuman? = response.body()
                if (serverResponse != null) {
                    //below is how you can get the list of result
                    val resultList: ArrayList<Pengumuman>? = response.body()!!.result
                    adapterPengumuman = AdapterPengumuman(context, resultList as MutableList<Pengumuman>)
                    adapterPengumuman.notifyDataSetChanged()
                    dua.adapter = adapterPengumuman

                }

            }

        })
//        mService.getpengumuman().enqueue(object : Callback<MutableList<Pengumuman>>{
//            override fun onFailure(call: Call<MutableList<Pengumuman>>, t: Throwable) {
//                print("Error : "+t.toString())
//            }
//
//            override fun onResponse(call: Call<MutableList<Pengumuman>>, response: Response<MutableList<Pengumuman>>) {
//                try {
//                    if (response!=null){
//                        print( "Data : "+response.body())
//                        adapterPengumuman = AdapterPengumuman(context, response.body() as MutableList<Pengumuman>)
//                        adapterPengumuman.notifyDataSetChanged()
//                        dua.adapter = adapterPengumuman
//                    }
//                }
//                catch (t : ExceptionInInitializerError){
//                    print( "Data : "+t.message)
//                }
//            }
//
//        })
    }


}