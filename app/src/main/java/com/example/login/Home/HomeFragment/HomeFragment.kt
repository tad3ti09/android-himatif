package com.example.login.Home.HomeFragment

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.opengl.Visibility
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.CardView
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import com.example.login.Adapter.AdapterBirth
import com.example.login.Adapter.AdapterPengumuman
import com.example.login.Api.API
import com.example.login.Api.Common
import com.example.login.Home.BphActivity
import com.example.login.Home.MahasiswaActivity
import com.example.login.Model.BirthModel
import com.example.login.Model.Pengumuman
import com.example.login.Model.ServerResponsePengumuman
import com.example.login.Model.ServiceResponseBirth

import com.example.login.R
import kotlinx.android.synthetic.main.fragment_pengumuman.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*


class HomeFragment : Fragment() {

    lateinit var cardView: CardView
    lateinit var cardView2: CardView
    lateinit var cardi: RecyclerView
    lateinit var textView: TextView
    lateinit var mService: API
    lateinit var layoutManager: LinearLayoutManager
    lateinit var adapterBirth: AdapterBirth

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        var view =  inflater.inflate(R.layout.fragment_home, container, false)

        cardView = view.findViewById(R.id.cardview)
        cardView2 = view.findViewById(R.id.cardview2)
        textView = view.findViewById(R.id.textError)
        cardView.setOnClickListener {
            var intent = Intent(context,MahasiswaActivity::class.java)
            startActivity(intent)
        }

        cardView2.setOnClickListener {

            var intent = Intent(context,BphActivity::class.java)
            startActivity(intent)
        }

        val sdf = SimpleDateFormat("MM-dd", Locale.getDefault())
        val currentDate = sdf.format(Date())

        cardi = view.findViewById(R.id.recycler)
        cardi.setHasFixedSize(true)
        layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        cardi.layoutManager = layoutManager

        mService = Common.retrofitService

        getBirthDay(currentDate.toString())

        return view


    }

    private fun getBirthDay(currentDate: String?) {
        mService.getAllBirth(currentDate.toString()).enqueue(object : Callback<ServiceResponseBirth>{
            override fun onFailure(call: Call<ServiceResponseBirth>, t: Throwable) {
                print("Error : "+t.message)
            }

            override fun onResponse(call: Call<ServiceResponseBirth>, response: Response<ServiceResponseBirth>) {
                val serverResponse: ServiceResponseBirth? = response.body()
                if (serverResponse != null) {
                    //below is how you can get the list of result
                    try {
                        val resultList: ArrayList<BirthModel>? = response.body()!!.result
                        adapterBirth = AdapterBirth(context, resultList as MutableList<BirthModel>)
                        adapterBirth.notifyDataSetChanged()
                        cardi.adapter = adapterBirth
                        textView.visibility = View.GONE
                    }
                    catch (t : Exception){
                        textView.visibility = View.VISIBLE
                    }

                }
                else{

                }

            }

        })
    }
}
