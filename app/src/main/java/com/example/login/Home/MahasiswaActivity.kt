package com.example.login.Home

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import android.widget.ImageView
import android.widget.TextView
import com.example.login.Adapter.AdapterMahasiswa
import com.example.login.Adapter.AdapterPengumuman
import com.example.login.Api.API
import com.example.login.Api.Common
import com.example.login.Model.*

import com.example.login.R
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class MahasiswaActivity : AppCompatActivity() {

    lateinit var maha:RecyclerView

    lateinit var mService: API
    lateinit var layoutManager: LinearLayoutManager
    lateinit var adapterMahasiswa: AdapterMahasiswa


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mahasiswa)
        maha = findViewById(R.id.maha)
        maha.setHasFixedSize(true)
        layoutManager = LinearLayoutManager(applicationContext)
        maha.layoutManager = layoutManager

        mService = Common.retrofitService

        getallmahasiswas()
    }

    private fun getallmahasiswas() {
        mService.getmahasiswas().enqueue(object : Callback<ServerResponseMahasiswa>{
            override fun onFailure(call: Call<ServerResponseMahasiswa>, t: Throwable) {
                print("Error : "+t.message)
            }

            override fun onResponse(call: Call<ServerResponseMahasiswa>, response: Response<ServerResponseMahasiswa>) {
                val serverResponse: ServerResponseMahasiswa? = response.body()
                if (serverResponse != null) {
                    //below is how you can get the list of result
                    val resultList: ArrayList<MahasiswaModel>? = response.body()!!.result
                    adapterMahasiswa = AdapterMahasiswa(applicationContext, resultList as MutableList<MahasiswaModel>)
                    adapterMahasiswa.notifyDataSetChanged()
                    maha.adapter = adapterMahasiswa

                }

            }

        })
    }
}
