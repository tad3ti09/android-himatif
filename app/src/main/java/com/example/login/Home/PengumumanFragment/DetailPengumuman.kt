package com.example.login.Home.PengumumanFragment

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import com.example.login.R
import com.squareup.picasso.Picasso

class DetailPengumuman : AppCompatActivity() {

    lateinit var pengumumanid: TextView
    lateinit var bphid: TextView
    lateinit var periodeid: TextView
    lateinit var detpe: TextView
    lateinit var taber: TextView
    lateinit var jupe: TextView
    lateinit var ImageListt: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_pengumuman)

        pengumumanid = findViewById(R.id.pengumumanid)
        var getIntent = getIntent()
        pengumumanid.text = getIntent.getStringExtra("pengumuman_id")
        bphid = findViewById(R.id.bphid)
        bphid.text = getIntent.getStringExtra("bph_id")
        periodeid = findViewById(R.id.periodeid)
        periodeid.text = getIntent.getStringExtra("periode_id")
        detpe = findViewById(R.id.detpe)
        detpe.text = getIntent.getStringExtra("detail_pengumuman")
        ImageListt = findViewById(R.id.lbImageListt)
        taber = findViewById(R.id.taber)
        taber.text = getIntent.getStringExtra("tanggal_akhir_pengumuman")
        jupe = findViewById(R.id.jupe)
        jupe.text = getIntent.getStringExtra("judul_pengumuman")


        Picasso.get().load("http://192.168.43.148/Himatif/foto_pengumuman/"+getIntent.getStringExtra("foto_pengumuman")).into(ImageListt)
    }
}
