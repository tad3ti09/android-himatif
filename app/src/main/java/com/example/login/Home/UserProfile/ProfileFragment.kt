package com.example.login.Home.UserProfile

import android.os.Bundle
import android.provider.ContactsContract
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import com.example.login.Api.ApiEndPoint
import com.example.login.Model.Mahasiswa

import com.example.login.R
import com.example.login.Session.SharedPrefManager
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView


class ProfileFragment : Fragment() {


    lateinit var share : SharedPrefManager
    lateinit var user : Mahasiswa

    // TODO: Rename and change types of parameters

    lateinit var image:CircleImageView
    lateinit var tvNama: TextView
    lateinit var tvemail: TextView
    lateinit var nim: TextView
    lateinit var tvAlamat: TextView
    lateinit var tvKelas: TextView
    lateinit var tvAngkatan: TextView
    lateinit var tvStatma: TextView

    lateinit var logout: Button






    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        var view= inflater.inflate(R.layout.fragment_profile, container, false)

        share = SharedPrefManager(context)
        user = share.getInstance(context).getMahasiswa()





        image = view.findViewById(R.id.imageUser)
        tvNama = view.findViewById(R.id.tvNama)
        tvemail = view.findViewById(R.id.tvEmail)
        nim = view.findViewById(R.id.nim)
        tvAlamat = view.findViewById(R.id.tvAlamat)
        tvKelas = view.findViewById(R.id.tvKelas)
        tvAngkatan = view.findViewById(R.id.tvAngkatan)
        tvStatma = view.findViewById(R.id.tvStatma)
        logout = view.findViewById(R.id.logout)

        tvNama.text = user.nama
        tvemail.text = user.email
        nim.text = user.nim
        tvAlamat.text = user.alamat
        tvKelas.text = user.kelas
        tvAngkatan.text = user.angkatan
        tvStatma.text = user.status_mahasiswa

        Picasso.get().load("http://adminhimatifapps.researchdiploma.id/foto_mahasiswa/"+user.foto_mahasiswa).into(image)


        logout.setOnClickListener {
            share.logout()
        }

        return view
    }


}
