package com.example.login.Home.KegiatanFragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.login.Adapter.AdapterKegiatan
import com.example.login.Api.API
import com.example.login.Api.Common
import com.example.login.Model.Kegiatan
import com.example.login.Model.ServerResponse
import com.example.login.R
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class KegiatanFragment : Fragment() {

    lateinit var sada: RecyclerView

    lateinit var mService: API
    lateinit var layoutManager: LinearLayoutManager
    lateinit var adapterKegiatan: AdapterKegiatan

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        var view = inflater.inflate(R.layout.fragment_kegiatan, container, false)
        sada = view.findViewById(R.id.sada)
        sada.setHasFixedSize(true)
        layoutManager = LinearLayoutManager(context)
        sada.layoutManager = layoutManager

        mService = Common.retrofitService

        getallkegiatan()
        return view
    }
    private fun getallkegiatan(){
        mService.getkegiatan().enqueue(object : Callback<ServerResponse>{
            override fun onFailure(call: Call<ServerResponse>, t: Throwable) {
                print("Error : "+t.message)
            }

            override fun onResponse(call: Call<ServerResponse>, response: Response<ServerResponse>) {
                val serverResponse: ServerResponse? = response.body()
                if (serverResponse != null) {
                    //below is how you can get the list of result
                    val resultList: ArrayList<Kegiatan>? = response.body()!!.result
                    adapterKegiatan = AdapterKegiatan(context, resultList as MutableList<Kegiatan>)
                    adapterKegiatan.notifyDataSetChanged()
                    sada.adapter = adapterKegiatan

                }

            }

        })
    }

//    private fun getallkegiatan() {
//
////        AndroidNetworking.get(API.Kegiatan)
//        mService.getkegiatan().enqueue(object : Callback<MutableList<Kegiatan>> {
//            override fun onFailure(call: Call<MutableList<Kegiatan>>, t: Throwable) {
//                print("Error : " + t.toString())
//            }
//
//            override fun onResponse(call: Call<MutableList<Kegiatan>>, response: Response<MutableList<Kegiatan>>) {
////                Log.d("horas", response.toString())
//                try {
//                    if (response != null) {
////
//                        try {
//                            val jsonData = JSONObject(response.body().toString())
//
////                        println("Status : "+jsonData.getString("status"))
//                            var currentObject: JSONArray ?= null
//                            val json = JSONObject(response.body().toString())
//                            Log.d("test : ",json.getString("status"))
////                        for (i in 0 until json.getJSONArray("DataKegiatan").length()) {
////                            currentObject = json.getJSONArray("DataKegiatan")
////
////                        }
//
//                            print("Data : "+currentObject.toString())
//
//                            adapterKegiatan = AdapterKegiatan(context, json.getJSONArray("DataKegiatan") as MutableList<Kegiatan>)
//                            adapterKegiatan.notifyDataSetChanged()
//                            sada.adapter = adapterKegiatan
//
//
//                        }catch (t : Exception){
//                            Log.d("Errorna : ", t.message)
//                        }
//
//                    }
//                } catch (t: ExceptionInInitializerError) {
//                    print("Data : " + t.message)
//                    println("EXCEPTION ERROR")
//                }
//            }
//
//        })
//    }


}
