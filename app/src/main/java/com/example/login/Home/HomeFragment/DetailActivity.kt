package com.example.login.Home.HomeFragment

import android.app.ProgressDialog
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.*
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import com.example.login.Api.ApiEndPoint
import com.example.login.Home.HomeActivity
import com.example.login.Model.Kegiatan
import com.example.login.Model.Mahasiswa
import com.example.login.R
import com.example.login.Session.SharedPrefManager
import com.squareup.picasso.Picasso
import org.json.JSONObject

class DetailActivity : AppCompatActivity() {

    lateinit var share : SharedPrefManager
    lateinit var user : Mahasiswa


    lateinit var kegiatanid : TextView
    lateinit var bphid : TextView
    lateinit var periodeid : TextView
    lateinit var namakeg : TextView
    lateinit var wakeg : TextView
    lateinit var tekeg : TextView
    lateinit var deskeg : TextView
    //lateinit var fokeg : TextView
    lateinit var taber : TextView
    lateinit var stakeg : TextView
    lateinit var penulis : TextView
    lateinit var ImageList: ImageView

    //kuisioner
    lateinit var opsi1: RadioGroup
    lateinit var opsi2: RadioGroup
    lateinit var opsi3: RadioGroup
    lateinit var komentar: EditText
    lateinit var submit: Button

    lateinit var pilihan1: String
    lateinit var pilihan2: String
    lateinit var pilihan3: String

    lateinit var Kegiatan_id: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)


        share = SharedPrefManager(applicationContext)
        user = share.getInstance(applicationContext).getMahasiswa()



        kegiatanid = findViewById(R.id.kegiatanid)
        var getIntent =  getIntent()
        kegiatanid.text = getIntent.getStringExtra("kegiatan_id")
        bphid = findViewById(R.id.bphid)
        bphid.text = getIntent.getStringExtra("bph_id")
        periodeid = findViewById(R.id.periodeid)
        periodeid.text = getIntent.getStringExtra("periode_id")
        namakeg = findViewById(R.id.namakeg)
        namakeg.text = getIntent.getStringExtra("nama_kegiatan")
        wakeg = findViewById(R.id.wakeg)
        wakeg.text = getIntent.getStringExtra("waktu_kegiatan")
        tekeg = findViewById(R.id.tekeg)
        tekeg.text = getIntent.getStringExtra("tempat_kegiatan")
        deskeg = findViewById(R.id.deskeg)
        deskeg.text = getIntent.getStringExtra("deskripsi_kegiatan")
//        fokeg = findViewById(R.id.fokeg)
        ImageList = findViewById(R.id.ImageList)
        taber = findViewById(R.id.taber)
        taber.text = getIntent.getStringExtra("tanggal_berakhir")
        stakeg = findViewById(R.id.stakeg)

        opsi1 = findViewById(R.id.opsi)
        opsi2 = findViewById(R.id.opsi2)
        opsi3 = findViewById(R.id.opsi3)
        komentar = findViewById(R.id.komentar)
        submit = findViewById(R.id.submit)
        Kegiatan_id = getIntent.getStringExtra("kegiatan_id")



        stakeg.text = getIntent.getStringExtra("status_kegiatan")
        penulis = findViewById(R.id.penulis)
        penulis.text = getIntent.getStringExtra("penulis")


        opsi1.setOnCheckedChangeListener(RadioGroup.OnCheckedChangeListener { group, checkedId ->
            val radio_button1 : RadioButton = findViewById(checkedId)
            pilihan1 = radio_button1.text.toString()
        })

        opsi2.setOnCheckedChangeListener(RadioGroup.OnCheckedChangeListener { group, checkedId ->
            val radio_button2 : RadioButton = findViewById(checkedId)
            pilihan2 = radio_button2.text.toString()
        })

        opsi3.setOnCheckedChangeListener(RadioGroup.OnCheckedChangeListener { group, checkedId ->
            val radio_button3 : RadioButton = findViewById(checkedId)
            pilihan3 = radio_button3.text.toString()
        })

        submit.setOnClickListener {
            if (komentar.getText().toString().equals("")){
            Toast.makeText(applicationContext,"Komentar Tidak Boleh Kosong",
                    Toast.LENGTH_SHORT).show()
            }
            else if(pilihan1 == null){
                Toast.makeText(applicationContext,"Pertanyaan 1 Belum Dijawab",
                        Toast.LENGTH_SHORT).show()
            }
            else if(pilihan2 == null){
                Toast.makeText(applicationContext,"Pertanyaan 2 Belum Dijawab",
                        Toast.LENGTH_SHORT).show()
            }
            else if(pilihan3 == null){
                Toast.makeText(applicationContext,"Pertanyaan 3 Belum Dijawab",
                        Toast.LENGTH_SHORT).show()
            }
            else{
                submitkui(pilihan1,pilihan2,pilihan3,komentar.getText().toString())
            }

        }

        Picasso.get().load("http://192.168.43.148/Himatif/foto_kegiatan/"+getIntent.getStringExtra("foto_kegiatan")).into(ImageList)
    }

    private fun submitkui(pilihan1: String, pilihan2: String, pilihan3: String, toString: String) {
        var loading = ProgressDialog(this)
        loading.setMessage("Mengirim Surat Kuisioner")
        loading.show()

//        println("id _customer : "+user.id_user)

        AndroidNetworking.post(ApiEndPoint.sendkuisioner)
                .addBodyParameter("kegiatan_id",Kegiatan_id)
                .addBodyParameter("mahasiswa_id",user.mahasiswa_id)
                .addBodyParameter("jab1", pilihan1)
                .addBodyParameter("jab2", pilihan2)
                .addBodyParameter("jab3", pilihan3)
                .addBodyParameter("jab4",toString)

                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(object: JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject?) {
                        var userJSON: JSONObject
                        loading.dismiss()
//                        Toast.makeText(applicationContext, response?.getString("message"), Toast.LENGTH_LONG).show()
                        if (response?.getString("message")?.contains("Kuisioner Berhasil Ditambah")!!){
                            Toast.makeText(applicationContext, response?.getString("message"), Toast.LENGTH_LONG).show()
                            val i = Intent(this@DetailActivity, HomeActivity::class.java)
                            startActivity(i)
                            finish()
                        }
                        else{
                            Toast.makeText(applicationContext, "Coba Lagi", Toast.LENGTH_LONG).show()
                        }
                    }

                    override fun onError(anError: ANError?) {
                        loading.dismiss()
                        Log.d("OneError : ",anError?.errorDetail?.toString())
                        Toast.makeText(this@DetailActivity,"Connection Failure", Toast.LENGTH_LONG).show()
                    }

                })
    }
}
