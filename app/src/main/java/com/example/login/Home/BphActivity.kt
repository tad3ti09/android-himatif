package com.example.login.Home

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import android.widget.ImageView
import android.widget.TextView
import com.example.login.Adapter.AdapterBph
import com.example.login.Adapter.AdapterMahasiswa
import com.example.login.Adapter.AdapterPengumuman
import com.example.login.Api.API
import com.example.login.Api.Common
import com.example.login.Model.*

import com.example.login.R
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class BphActivity : AppCompatActivity() {

    lateinit var bph:RecyclerView

    lateinit var mService: API
    lateinit var layoutManager: LinearLayoutManager
    lateinit var adapterBph: AdapterBph


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bph)
        bph = findViewById(R.id.bph)
        bph.setHasFixedSize(true)
        layoutManager = LinearLayoutManager(applicationContext)
        bph.layoutManager = layoutManager

        mService = Common.retrofitService

        getallbph()
    }

    private fun getallbph() {

        mService.getbph().enqueue(object : Callback<ServerResponseBph>{
            override fun onFailure(call: Call<ServerResponseBph>, t: Throwable) {
                print("Error : "+t.message)
            }

            override fun onResponse(call: Call<ServerResponseBph>, response: Response<ServerResponseBph>) {
                val serverResponse: ServerResponseBph? = response.body()
                if (serverResponse != null) {
                    //below is how you can get the list of result
                    val resultList: ArrayList<BphModel>? = response.body()!!.result
                    adapterBph = AdapterBph(applicationContext, resultList as MutableList<BphModel>)
                    adapterBph.notifyDataSetChanged()
                    bph.adapter = adapterBph

                }

            }

        })
    }
}

