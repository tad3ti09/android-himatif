package com.example.login.Home.UangFragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import com.example.login.Adapter.AdapterPengumuman
import com.example.login.Adapter.AdapterUang
import com.example.login.Api.API
import com.example.login.Api.Common
import com.example.login.Model.Pengumuman
import com.example.login.Model.ServerResponseKas
import com.example.login.Model.ServerResponsePengumuman
import com.example.login.Model.Uang

import com.example.login.R
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class UangFragment : Fragment() {

    lateinit var lbJulbarList: TextView
    lateinit var lbBuatList: TextView
    lateinit var lbAkhirList: TextView
    lateinit var lbDeskripsiList: TextView
    lateinit var lbStatusList: TextView
    lateinit var lbNamaPembayarList: TextView


    lateinit var tolu:RecyclerView

    lateinit var mService: API
    lateinit var layoutManager: LinearLayoutManager
    lateinit var adapterUang: AdapterUang

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        var view= inflater.inflate(R.layout.fragment_uang, container, false)
        tolu = view.findViewById(R.id.tolu)
        tolu.setHasFixedSize(true)
        layoutManager = LinearLayoutManager(context)
        tolu.layoutManager = layoutManager

        mService = Common.retrofitService

        getalluang()
        return view


        lbJulbarList = view.findViewById(R.id.lbJulbarList)
        lbBuatList = view.findViewById(R.id.lbBuatList)
        lbAkhirList = view.findViewById(R.id.lbAkhirList)
        lbDeskripsiList = view.findViewById(R.id.lbDeskripsiList)
        lbStatusList = view.findViewById(R.id.lbStatusList)
        lbNamaPembayarList = view.findViewById(R.id.lbNamaPembayarList)



    }

    private fun getalluang() {
        mService.getuang().enqueue(object : Callback<ServerResponseKas>{
            override fun onFailure(call: Call<ServerResponseKas>, t: Throwable) {
                print("Error : "+t.message)
            }

            override fun onResponse(call: Call<ServerResponseKas>, response: Response<ServerResponseKas>) {
                val serverResponse: ServerResponseKas? = response.body()
                if (serverResponse != null) {
                    //below is how you can get the list of result
                    val resultList: ArrayList<Uang>? = response.body()!!.result
                    adapterUang = AdapterUang(context, resultList as MutableList<Uang>)
                    adapterUang.notifyDataSetChanged()
                    tolu.adapter = adapterUang

                }

            }

        })
    }


}