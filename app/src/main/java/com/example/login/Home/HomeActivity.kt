package com.example.login.Home

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.FragmentTransaction
import com.example.login.Home.HomeFragment.HomeFragment
import com.example.login.Home.KegiatanFragment.KegiatanFragment
import com.example.login.Home.PengumumanFragment.PengumumanFragment
import com.example.login.Home.UangFragment.UangFragment
import com.example.login.Home.UserProfile.ProfileFragment
import com.example.login.R

class HomeActivity : AppCompatActivity() {
    lateinit var home : KegiatanFragment
    lateinit var homes : HomeFragment
    lateinit var pengumuman : PengumumanFragment
    lateinit var uang : UangFragment
    lateinit var profile: ProfileFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        val bottomNavigationView : BottomNavigationView = findViewById(R.id.navigation)

        bottomNavigationView.setOnNavigationItemSelectedListener { item ->
            when(item.itemId){
                R.id.navigation_home -> {
                    home = KegiatanFragment()
                    supportFragmentManager
                            .beginTransaction()
                            .replace(R.id.content, home)
                            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                            .commit()
                }
                R.id.navigation_pengumuman -> {
                    pengumuman = PengumumanFragment()
                    supportFragmentManager
                            .beginTransaction()
                            .replace(R.id.content, pengumuman)
                            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                            .commit()
                }
                R.id.navigation_uang -> {
                    uang = UangFragment()
                    supportFragmentManager
                            .beginTransaction()
                            .replace(R.id.content, uang)
                            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                            .commit()
                }
                R.id.profile -> {
                    profile = ProfileFragment()
                    supportFragmentManager
                            .beginTransaction()
                            .replace(R.id.content, profile)
                            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                            .commit()
                }
                R.id.navigation_dash->{
                    homes = HomeFragment()
                    supportFragmentManager
                            .beginTransaction()
                            .replace(R.id.content, homes)
                            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                            .commit()
                }
            }
            true
        }

        homes = HomeFragment()
        supportFragmentManager
                .beginTransaction()
                .replace(R.id.content, homes)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit()


    }
}
