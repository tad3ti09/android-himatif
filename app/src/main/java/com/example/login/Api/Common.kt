package com.example.login.Api

object Common {
    private val BASE_URL = ApiEndPoint.SERVER
    val retrofitService: API
        get() = RestClient.getClient(BASE_URL)!!.create(API::class.java)
}