package com.example.login.Api

object AppVar {
    //URL to our login.php file, url bisa diganti sesuai dengan alamat server kita
    const val LOGIN_URL = "http://192.168.43.148/TA/login.php"

    //Keys for email and password as defined in our $_POST['key'] in login.php
    const val KEY_EMAIL = "email"
    const val KEY_PASSWORD = "password"

    //If server response is equal to this that means login is successful
    const val LOGIN_SUCCESS = "success"
}