package com.example.login.Api

import com.example.login.Model.*
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface API {
    @GET("kegiatan")
    fun getkegiatan(): Call<ServerResponse>

    @GET("pengumuman")
    fun getpengumuman(): Call<ServerResponsePengumuman>

    @GET("kas")
    fun getuang(): Call<ServerResponseKas>

    @GET("mahasiswa")
    fun getmahasiswas(): Call<ServerResponseMahasiswa>

    @GET("bph")
    fun getbph(): Call<ServerResponseBph>

    @GET("birth/{birth_date}")
    fun getAllBirth(@Path("birth_date") date:String):Call<ServiceResponseBirth>

}

