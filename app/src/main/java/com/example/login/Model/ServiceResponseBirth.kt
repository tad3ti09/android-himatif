package com.example.login.Model

import com.google.gson.annotations.SerializedName

class ServiceResponseBirth {
    @SerializedName("DataBirth")
    private var mResult: ArrayList<BirthModel>? = null
    var result: ArrayList<BirthModel>?
        get() = mResult
        set(result) {
            mResult = result
        }
}