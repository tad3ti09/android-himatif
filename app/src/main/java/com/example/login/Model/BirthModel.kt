package com.example.login.Model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class BirthModel {
    @SerializedName("mahasiswa_id")
    @Expose
    var mahasiswa_id: String? =null
    @SerializedName("nama")
    @Expose
    var nama: String? =null
    @SerializedName("foto_mahasiswa")
    @Expose
    var foto_mahasiswa: String? =null
}