package com.example.login.Model

import com.google.gson.annotations.SerializedName

class ServerResponseKas {
    @SerializedName("DataKas")
    private var mResult: ArrayList<Uang>? = null
    var result: ArrayList<Uang>?
        get() = mResult
        set(result) {
            mResult = result
        }
}
