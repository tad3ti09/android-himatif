package com.example.login.Model

import com.google.gson.annotations.SerializedName

class ServerResponseBph {
    @SerializedName("DataBph")
    private var mResult: ArrayList<BphModel>? = null
    var result: ArrayList<BphModel>?
        get() = mResult
        set(result) {
            mResult = result
        }
}
