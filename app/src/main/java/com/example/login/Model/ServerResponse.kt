package com.example.login.Model

import com.google.gson.annotations.SerializedName


class ServerResponse {
    @SerializedName("DataKegiatan")
    private var mResult: ArrayList<Kegiatan>? = null
    var result: ArrayList<Kegiatan>?
        get() = mResult
        set(result) {
            mResult = result
        }

}