package com.example.login.Model

import com.google.gson.annotations.SerializedName

class ServerResponseMahasiswa {

    @SerializedName("DataMahasiswa")
    private var mResult: ArrayList<MahasiswaModel>? = null
    var result: ArrayList<MahasiswaModel>?
        get() = mResult
        set(result) {
            mResult = result
        }
}
