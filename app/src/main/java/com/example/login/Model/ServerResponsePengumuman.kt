package com.example.login.Model

import com.google.gson.annotations.SerializedName

class ServerResponsePengumuman {
    @SerializedName("DataPengumuman")
    private var mResult: ArrayList<Pengumuman>? = null
    var result: ArrayList<Pengumuman>?
        get() = mResult
        set(result) {
            mResult = result
        }
}