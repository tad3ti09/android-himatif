package com.example.login.Model
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



class BphModel {
    @SerializedName("bph_id")
    @Expose
    var bph_id: String? =null
    @SerializedName("periode_id")
    @Expose
    var periode_id: String? =null
    @SerializedName("mahasiswa_id")
    @Expose
    var mahasiswa_id: String? =null
    @SerializedName("jabatan")
    @Expose
    var jabatan: String? =null
    @SerializedName("nama")
    @Expose
    var nama: String? =null
    @SerializedName("username")
    @Expose
    var username: String? =null
    @SerializedName("password")
    @Expose
    var password: String? =null
    @SerializedName("nim")
    @Expose
    var nim: String? =null
    @SerializedName("kelas")
    @Expose
    var kelas: String? =null
    @SerializedName("angkatan")
    @Expose
    var angkatan: String? =null
    @SerializedName("alamat")
    @Expose
    var alamat: String? =null
    @SerializedName("email")
    @Expose
    var email: String? =null
    @SerializedName("status_mahasiswa")
    @Expose
    var status_mahasiswa: String? =null
    @SerializedName("tempat_lahir")
    @Expose
    var tempat_lahir: String? =null
    @SerializedName("tanggal_lahir")
    @Expose
    var tanggal_lahir: String? =null
    @SerializedName("role")
    @Expose
    var role: String? =null
    @SerializedName("foto_mahasiswa")
    @Expose
    var foto_mahasiswa: String? =null
    @SerializedName("periode_awal")
    @Expose
    var periode_awal: String? =null
    @SerializedName("periode_akhir")
    @Expose
    var periode_akhir: String? =null
    @SerializedName("status")
    @Expose
    var status: String? =null
}