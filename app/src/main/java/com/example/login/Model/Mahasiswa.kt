package com.example.login.Model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Mahasiswa {
    var mahasiswa_id:String=""
    var nama:String=""
    var username:String=""
    var password:String=""
    var nim:String=""
    var kelas:String=""
    var angkatan:String=""
    var alamat:String=""
    var email:String=""
    var status_mahasiswa:String=""
    var tempat_lahir:String=""
    var tanggal_lahir:String=""
    var role:String=""
    var foto_mahasiswa:String=""

    constructor(mahasiswa_id: String, nama: String, username: String, password: String, nim: String,
                kelas: String, angkatan: String, alamat: String, email: String, status_mahasiswa:
                String, tempat_lahir: String, tanggal_lahir: String, role: String, foto_mahasiswa: String) {
        this.mahasiswa_id = mahasiswa_id
        this.nama = nama
        this.username = username
        this.password = password
        this.nim = nim
        this.kelas = kelas
        this.angkatan = angkatan
        this.alamat = alamat
        this.email = email
        this.status_mahasiswa = status_mahasiswa
        this.tempat_lahir = tempat_lahir
        this.tanggal_lahir = tanggal_lahir
        this.role = role
        this.foto_mahasiswa = foto_mahasiswa
    }
}