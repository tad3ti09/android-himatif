package com.example.login.Model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Pengumuman {
    @SerializedName("pengumuman_id")
    @Expose
    var pengumuman_id: String? =null
    @SerializedName("bph_id")
    @Expose
    var bph_id: String? = null
    @SerializedName("periode_id")
    @Expose
    var periode_id: String? = null
    @SerializedName("detail_pengumuman")
    @Expose
    var detail_pengumuman: String? = null
    @SerializedName("tanggal_akhir_pengumuman")
    @Expose
    var tanggal_akhir_pengumuman: String? = null
    @SerializedName("judul_pengumuman")
    @Expose
    var judul_pengumuman: String? = null
    @SerializedName("foto_pengumuman")
    @Expose
    var foto_pengumuman: String? = null

}