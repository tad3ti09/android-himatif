package com.example.login.Model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Kegiatan{
    @SerializedName("kegiatan_id")
    @Expose
    var kegiatan_id: String? =null
    @SerializedName("bph_id")
    @Expose
    var bph_id: String? =null
    @SerializedName("periode_id")
    @Expose
    var periode_id: String? =null
    @SerializedName("nama_kegiatan")
    @Expose
    var nama_kegiatan: String? =null
    @SerializedName("waktu_kegiatan")
    @Expose
    var waktu_kegiatan: String? =null
    @SerializedName("tempat_kegiatan")
    @Expose
    var tempat_kegiatan: String? =null
    @SerializedName("deskripsi_kegiatan")
    @Expose
    var deskripsi_kegiatan: String? =null
    @SerializedName("foto_kegiatan")
    @Expose
    var foto_kegiatan: String? =null
    @SerializedName("tanggal_berakhir")
    @Expose
    var tanggal_berakhir: String? =null
    @SerializedName("status_kegiatan")
    @Expose
    var status_kegiatan: String? =null
    @SerializedName("penulis")
    @Expose
    var penulis: String? =null
}