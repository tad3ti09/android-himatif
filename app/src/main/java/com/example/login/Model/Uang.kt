package com.example.login.Model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Uang {
    @SerializedName("uang_kas_id")
    @Expose
    var uang_kas_id: String? =null
    @SerializedName("bph_id")
    @Expose
    var bph_id: String? =null
    @SerializedName("periode_id")
    @Expose
    var periode_id: String? =null
    @SerializedName("jumlah_bayar")
    @Expose
    var jumlah_bayar: String? =null
    @SerializedName("tanggal_dibuat")
    @Expose
    var tanggal_dibuat: String? =null
    @SerializedName("tanggal_berakhir")
    @Expose
    var tanggal_berakhir: String? =null
    @SerializedName("deskripsi_pembayaran")
    @Expose
    var deskripsi_pembayaran: String? = null
    @SerializedName("status_pembayaran")
    @Expose
    var status_pembayaran: String? =null
    @SerializedName("mahasiswa_id")
    @Expose
    var mahasiswa_id: String? =null
    @SerializedName("nama")
    @Expose
    var Nama_pemabayar: String? =null
}