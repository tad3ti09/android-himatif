package com.example.login.Adapter

import android.content.Context
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.example.login.Model.Mahasiswa
import com.example.login.Model.MahasiswaModel
import com.example.login.R
import com.squareup.picasso.Picasso

class AdapterMahasiswa(private val context: Context?, private val MahasiswaLIst: MutableList<MahasiswaModel>): RecyclerView.Adapter<AdapterMahasiswa.MyViewHolder>(){
    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var lbIDMahasiswaList: TextView?= null
        var lbNamaList: TextView?= null
        var lbUsernameList: TextView?= null
        var lbPasswordList: TextView?= null
        var lbNimList: TextView?= null
        var lbKelasList: TextView?= null
        var lbAngkatanList: TextView?= null
        var lbAlamatList: TextView?= null
        var lbEmailList: TextView?= null
        var lbStatmaList: TextView?= null
        var lbTelatList: TextView?= null
        var lbTalaList: TextView?= null
        var lbRoleList: TextView?= null
        var lbImageList: ImageView?= null

        var cardView: CardView?= null

        init {
            cardView= itemView.findViewById(R.id.masa)
            lbImageList = itemView.findViewById(R.id.lbImageList)
            lbIDMahasiswaList = itemView.findViewById(R.id.lbIDMahasiswa)
            lbNamaList = itemView.findViewById(R.id.lbNamaList)
            lbUsernameList = itemView.findViewById(R.id.lbUsernameList)
            lbPasswordList = itemView.findViewById(R.id.lbPasswordList)
            lbNimList = itemView.findViewById(R.id.lbNimList)
            lbKelasList = itemView.findViewById(R.id.lbKelasList)
            lbAngkatanList = itemView.findViewById(R.id.lbAngkatanList)
            lbAlamatList = itemView.findViewById(R.id.lbAlamatList)
            lbEmailList = itemView.findViewById(R.id.lbEmailList)
            lbStatmaList = itemView.findViewById(R.id.lbStatmaList)
            lbTelatList = itemView.findViewById(R.id.lbTelaList)
            lbTalaList = itemView.findViewById(R.id.lbTalaList)
            lbRoleList = itemView.findViewById(R.id.lbRoleList)


        }

    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): MyViewHolder {
        var itemView= LayoutInflater.from(context).inflate(R.layout.mahasiswa_list, p0, false)
        return MyViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return MahasiswaLIst.size
    }

    override fun onBindViewHolder(p0: MyViewHolder, p1: Int) {
        p0.lbIDMahasiswaList!!.text = MahasiswaLIst[p1].mahasiswa_id
        p0.lbNamaList!!.text = MahasiswaLIst[p1].nama
        p0.lbUsernameList!!.text = MahasiswaLIst[p1].username
        p0.lbPasswordList!!.text = MahasiswaLIst[p1].password
        p0.lbNimList!!.text = MahasiswaLIst[p1].nim
        p0.lbKelasList!!.text = MahasiswaLIst[p1].kelas
        p0.lbAngkatanList!!.text = MahasiswaLIst[p1].angkatan
        p0.lbAlamatList!!.text = MahasiswaLIst[p1].alamat
        p0.lbEmailList!!.text = MahasiswaLIst[p1].email
        p0.lbStatmaList!!.text = MahasiswaLIst[p1].status_mahasiswa
        p0.lbTelatList!!.text = MahasiswaLIst[p1].tempat_lahir
        p0.lbTalaList!!.text = MahasiswaLIst[p1].tanggal_lahir
        p0.lbRoleList!!.text = MahasiswaLIst[p1].role

        Picasso.get().load("https://adminhimatifapps.researchdiploma.id/foto_mahasiswa/"+MahasiswaLIst[p1].foto_mahasiswa).into(p0.lbImageList)



    }
}