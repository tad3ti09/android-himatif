package com.example.login.Adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.example.login.Model.Uang
import com.example.login.R

class AdapterUang(private val context: Context?, private val UangLIst:MutableList<Uang>): RecyclerView.Adapter<AdapterUang.MyViewHolder>(){
    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var lbIdlist:TextView?= null
        var lbBPHlist:TextView?= null
        var lbPeriodeList:TextView?= null
        var lbJulbarList:TextView?= null
        var lbBuatList:TextView?= null
        var lbAkhirList:TextView?= null
//        var lbImageList:ImageView?= null
        var lbDeskripsiList:TextView?= null
        var lbStatusList:TextView?=null
        var lbIDMahasiswaList:TextView?=null
        var lbNamaPembayar:TextView?= null

        init {
            lbIdlist = itemView.findViewById(R.id.lbIdList)
            lbBPHlist = itemView.findViewById(R.id.lbBPHList)
            lbPeriodeList = itemView.findViewById(R.id.lbPeriodeList)
            lbJulbarList = itemView.findViewById(R.id.lbJulbarList)
            lbBuatList = itemView.findViewById(R.id.lbBuatList)
            lbAkhirList = itemView.findViewById(R.id.lbAkhirList)
            lbDeskripsiList = itemView.findViewById(R.id.lbDeskripsiList)
            lbStatusList = itemView.findViewById(R.id.lbStatusList)
            lbIDMahasiswaList = itemView.findViewById(R.id.lbIDMahasiswaList)
            lbNamaPembayar = itemView.findViewById(R.id.lbNamaPembayarList)





//            lbImageList = itemView.findViewById(R.id.lbImageList)


        }
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): MyViewHolder {
        var itemView= LayoutInflater.from(context).inflate(R.layout.uang_list, p0, false)
        return MyViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return UangLIst.size
    }

    override fun onBindViewHolder(p0: MyViewHolder, p1: Int) {
        p0.lbIdlist!!.text = UangLIst[p1].uang_kas_id
        p0.lbBPHlist!!.text = UangLIst[p1].bph_id
        p0.lbPeriodeList!!.text = UangLIst[p1].periode_id
        p0.lbJulbarList!!.text = UangLIst[p1].jumlah_bayar
        p0.lbBuatList!!.text = UangLIst[p1].tanggal_dibuat
        p0.lbAkhirList!!.text = UangLIst[p1].tanggal_berakhir
        p0.lbDeskripsiList!!.text = UangLIst[p1].deskripsi_pembayaran
        p0.lbStatusList!!.text = UangLIst[p1].status_pembayaran
        p0.lbIDMahasiswaList!!.text = UangLIst[p1].mahasiswa_id
        p0.lbNamaPembayar!!.text = UangLIst[p1].Nama_pemabayar





    }

}