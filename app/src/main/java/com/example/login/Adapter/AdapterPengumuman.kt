package com.example.login.Adapter

import android.content.Context
import android.content.Intent
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.example.login.Home.PengumumanFragment.DetailPengumuman
import com.example.login.Model.Pengumuman
import com.example.login.R
import com.squareup.picasso.Picasso

class AdapterPengumuman(private val context: Context?, private val PengumumanLIst:MutableList<Pengumuman>): RecyclerView.Adapter<AdapterPengumuman.MyViewHolder>(){
    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var lbIdlist:TextView?= null
        var lbBPHlist:TextView?= null
        var lbPeriodeList:TextView?= null
        var lbDetailList:TextView?= null
        var lbWaktuakhirList:TextView?= null
        var lbJudulList:TextView?= null
        var lbImageList:ImageView?= null

        var cardView:CardView?= null

        init {
            cardView = itemView.findViewById(R.id.pengumumandetail)
            lbIdlist = itemView.findViewById(R.id.lbIdList)
            lbBPHlist = itemView.findViewById(R.id.lbBPHList)
            lbPeriodeList = itemView.findViewById(R.id.lbPeriodeList)
            lbDetailList = itemView.findViewById(R.id.lbDetailList)
            lbWaktuakhirList = itemView.findViewById(R.id.lbWaktuakhirList)
            lbJudulList = itemView.findViewById(R.id.lbJudulList)
            lbImageList = itemView.findViewById(R.id.lbImageList)


//            lbImageList = itemView.findViewById(R.id.lbImageList)


        }
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): MyViewHolder {
        var itemView= LayoutInflater.from(context).inflate(R.layout.pengumuman_list, p0, false)
        return MyViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return PengumumanLIst.size
    }

    override fun onBindViewHolder(p0: MyViewHolder, p1: Int) {
        p0.lbIdlist!!.text = PengumumanLIst[p1].pengumuman_id
        p0.lbBPHlist!!.text = PengumumanLIst[p1].bph_id
        p0.lbPeriodeList!!.text = PengumumanLIst[p1].periode_id
        p0.lbDetailList!!.text = PengumumanLIst[p1].detail_pengumuman
        p0.lbWaktuakhirList!!.text = PengumumanLIst[p1].tanggal_akhir_pengumuman
        p0.lbJudulList!!.text = PengumumanLIst[p1].judul_pengumuman

        Picasso.get().load("https://adminhimatifapps.researchdiploma.id/foto_pengumuman/"+PengumumanLIst[p1].foto_pengumuman).into(p0.lbImageList)
        p0.cardView!!.setOnClickListener {
            var intent = Intent(context,DetailPengumuman::class.java)
            intent.putExtra("foto_pengumuman", PengumumanLIst[p1].foto_pengumuman)
            intent.putExtra("pengumuman_id", PengumumanLIst[p1].pengumuman_id)
            intent.putExtra("bph_id", PengumumanLIst[p1].bph_id)
            intent.putExtra("periode_id", PengumumanLIst[p1].periode_id)
            intent.putExtra("detail_pengumuman", PengumumanLIst[p1].detail_pengumuman)
            intent.putExtra("tanggal_akhir_pengumuman", PengumumanLIst[p1].tanggal_akhir_pengumuman)
            intent.putExtra("judul_pengumuman", PengumumanLIst[p1].judul_pengumuman)
            context!!.startActivity(intent)
        }
    }

}