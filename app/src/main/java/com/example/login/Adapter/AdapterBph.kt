package com.example.login.Adapter

import android.content.Context
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.example.login.Model.BphModel
import com.example.login.R
import com.squareup.picasso.Picasso

class AdapterBph(private val context: Context?, private val BphLIst: MutableList<BphModel>): RecyclerView.Adapter<AdapterBph.MyViewHolder>(){
    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var lbIDBph: TextView?= null
        var lbIDPeriode: TextView?= null
        var lbIDMahasiswaList: TextView?= null
        var lbJabatanList: TextView?= null
        var lbNamaList: TextView?= null
        var lbUsernameList: TextView?= null
        var lbPasswordList: TextView?= null
        var lbNimList: TextView?= null
        var lbKelasList: TextView?= null
        var lbAngkatanList: TextView?= null
        var lbAlamatList: TextView?= null
        var lbEmailList: TextView?= null
        var lbStatmaList: TextView?= null
        var lbTelatList: TextView?= null
        var lbTalaList: TextView?= null
        var lbRoleList: TextView?= null
        var lbImageList: ImageView?= null
        var lbPeawList: TextView?= null
        var lbPeakList: TextView?= null
        var lbStatpeList: TextView?= null

        var cardView: CardView?= null

        init {
            cardView= itemView.findViewById(R.id.mana)
            lbImageList = itemView.findViewById(R.id.lbImageList)
            lbIDBph = itemView.findViewById(R.id.lbIDBph)
            lbIDPeriode = itemView.findViewById(R.id.lbIDPeriode)
            lbIDMahasiswaList = itemView.findViewById(R.id.lbIDMahasiswa)
            lbJabatanList = itemView.findViewById(R.id.lbJabatanList)
            lbNamaList = itemView.findViewById(R.id.lbNamaList)
            lbUsernameList = itemView.findViewById(R.id.lbUsernameList)
            lbPasswordList = itemView.findViewById(R.id.lbPasswordList)
            lbNimList = itemView.findViewById(R.id.lbNimList)
            lbKelasList = itemView.findViewById(R.id.lbKelasList)
            lbAngkatanList = itemView.findViewById(R.id.lbAngkatanList)
            lbAlamatList = itemView.findViewById(R.id.lbAlamatList)
            lbEmailList = itemView.findViewById(R.id.lbEmailList)
            lbStatmaList = itemView.findViewById(R.id.lbStatmaList)
            lbTelatList = itemView.findViewById(R.id.lbTelaList)
            lbTalaList = itemView.findViewById(R.id.lbTalaList)
            lbRoleList = itemView.findViewById(R.id.lbRoleList)
            lbPeawList = itemView.findViewById(R.id.lbPeawList)
            lbPeakList = itemView.findViewById(R.id.lbPeakList)
            lbStatpeList = itemView.findViewById(R.id.lbStatpeList)


        }
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): MyViewHolder {
        var itemView= LayoutInflater.from(context).inflate(R.layout.bph_list, p0, false)
        return MyViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return BphLIst.size
    }

    override fun onBindViewHolder(p0: MyViewHolder, p1: Int) {
        p0.lbIDBph!!.text = BphLIst[p1].bph_id
        p0.lbIDPeriode!!.text = BphLIst[p1].periode_id
        p0.lbIDMahasiswaList!!.text = BphLIst[p1].mahasiswa_id
        p0.lbJabatanList!!.text = BphLIst[p1].jabatan
        p0.lbNamaList!!.text = BphLIst[p1].nama
        p0.lbUsernameList!!.text = BphLIst[p1].username
        p0.lbPasswordList!!.text = BphLIst[p1].password
        p0.lbNimList!!.text = BphLIst[p1].nim
        p0.lbKelasList!!.text = BphLIst[p1].kelas
        p0.lbAngkatanList!!.text = BphLIst[p1].angkatan
        p0.lbAlamatList!!.text = BphLIst[p1].alamat
        p0.lbEmailList!!.text = BphLIst[p1].email
        p0.lbStatmaList!!.text = BphLIst[p1].status_mahasiswa
        p0.lbTelatList!!.text = BphLIst[p1].tempat_lahir
        p0.lbTalaList!!.text = BphLIst[p1].tanggal_lahir
        p0.lbRoleList!!.text = BphLIst[p1].role
        p0.lbPeawList!!.text = BphLIst[p1].periode_awal
        p0.lbPeakList!!.text = BphLIst[p1].periode_akhir
        p0.lbStatpeList!!.text = BphLIst[p1].status

        Picasso.get().load("https://adminhimatifapps.researchdiploma.id/foto_mahasiswa/"+BphLIst[p1].foto_mahasiswa).into(p0.lbImageList)


    }
}