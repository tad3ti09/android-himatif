package com.example.login.Adapter

import android.content.Context
import android.content.Intent
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.example.login.Home.HomeFragment.DetailActivity
import com.example.login.Model.Kegiatan
import com.example.login.R
import com.squareup.picasso.Picasso

class AdapterKegiatan(private val context: Context?, private val KegiatanLIst:MutableList<Kegiatan>): RecyclerView.Adapter<AdapterKegiatan.MyViewHolder>(){
    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var lbIdlist:TextView?= null
        var lbBPHlist:TextView?= null
        var lbPeriodeList:TextView?= null
        var lbNamaList:TextView?= null
        var lbWaktuList:TextView?= null
        var lbTempatList:TextView?= null
        var lbDeskripsiList:TextView?= null
        var lbImageList:ImageView?= null
        var lbBerakhirList:TextView?= null
        var lbStatusList:TextView?= null
        var lbPenulisList:TextView?= null


        var cardView:CardView?= null

        init {
            cardView= itemView.findViewById(R.id.detailKegiatan)
            lbIdlist = itemView.findViewById(R.id.lbIdList)
            lbBPHlist = itemView.findViewById(R.id.lbBPHList)
            lbPeriodeList = itemView.findViewById(R.id.lbPeriodeList)
            lbNamaList = itemView.findViewById(R.id.lbNamaList)
            lbWaktuList = itemView.findViewById(R.id.lbWaktuList)
            lbTempatList = itemView.findViewById(R.id.lbTempatList)
            lbDeskripsiList = itemView.findViewById(R.id.lbDeskripsiList)
            lbImageList = itemView.findViewById(R.id.lbImageList)
            lbBerakhirList = itemView.findViewById(R.id.lbBerakhirList)
            lbStatusList = itemView.findViewById(R.id.lbStatusList)
            lbPenulisList = itemView.findViewById(R.id.lbPenulisList)
        }
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): MyViewHolder {
        var itemView= LayoutInflater.from(context).inflate(R.layout.kegiatan_list, p0, false)
        return MyViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return KegiatanLIst.size
    }

    override fun onBindViewHolder(p0: MyViewHolder, p1: Int) {
        p0.lbIdlist!!.text = KegiatanLIst[p1].kegiatan_id
        p0.lbBPHlist!!.text = KegiatanLIst[p1].bph_id
        p0.lbPeriodeList!!.text = KegiatanLIst[p1].periode_id
        p0.lbNamaList!!.text = KegiatanLIst[p1].nama_kegiatan
        p0.lbWaktuList!!.text = KegiatanLIst[p1].waktu_kegiatan
        p0.lbTempatList!!.text = KegiatanLIst[p1].tempat_kegiatan
        p0.lbDeskripsiList!!.text = KegiatanLIst[p1].deskripsi_kegiatan
        p0.lbBerakhirList!!.text = KegiatanLIst[p1].tanggal_berakhir
        p0.lbStatusList!!.text = KegiatanLIst[p1].status_kegiatan
        p0.lbPenulisList!!.text = KegiatanLIst[p1].penulis

        Picasso.get().load("https://adminhimatifapps.researchdiploma.id/foto_kegiatan/"+KegiatanLIst[p1].foto_kegiatan).into(p0.lbImageList)
        p0.cardView!!.setOnClickListener {
            var intent = Intent(context,DetailActivity::class.java)
            intent.putExtra("foto_kegiatan", KegiatanLIst[p1].foto_kegiatan)
            intent.putExtra("kegiatan_id", KegiatanLIst[p1].kegiatan_id)
            intent.putExtra("bph_id", KegiatanLIst[p1].bph_id)
            intent.putExtra("periode_id", KegiatanLIst[p1].periode_id)
            intent.putExtra("nama_kegiatan", KegiatanLIst[p1].nama_kegiatan)
            intent.putExtra("waktu_kegiatan", KegiatanLIst[p1].waktu_kegiatan)
            intent.putExtra("tempat_kegiatan", KegiatanLIst[p1].tempat_kegiatan)
            intent.putExtra("deskripsi_kegiatan", KegiatanLIst[p1].deskripsi_kegiatan)
            intent.putExtra("tanggal_berakhir", KegiatanLIst[p1].tanggal_berakhir)
            intent.putExtra("status_kegiatan", KegiatanLIst[p1].status_kegiatan)
            intent.putExtra("penulis", KegiatanLIst[p1].penulis)
            context!!.startActivity(intent)
        }

    }

}