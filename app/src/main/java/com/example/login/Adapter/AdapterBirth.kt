package com.example.login.Adapter

import android.content.Context
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.example.login.Model.Mahasiswa
import com.example.login.Model.BirthModel
import com.example.login.R
import com.squareup.picasso.Picasso

class AdapterBirth(private val context: Context?, private val BirthLIst: MutableList<BirthModel>): RecyclerView.Adapter<AdapterBirth.MyViewHolder>(){
    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var lbIDMahasiswaList: TextView?= null
        var lbNamaList: TextView?= null
        var lbImageList: ImageView?= null

        var cardView: CardView?= null

        init {
            cardView= itemView.findViewById(R.id.cardview)
            lbImageList = itemView.findViewById(R.id.lbImageList)
            lbIDMahasiswaList = itemView.findViewById(R.id.lbIDMahasiswa)
            lbNamaList = itemView.findViewById(R.id.lbNamaList)
        }

    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): MyViewHolder {
        var itemView= LayoutInflater.from(context).inflate(R.layout.birth_item, p0, false)
        return MyViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return BirthLIst.size
    }

    override fun onBindViewHolder(p0: MyViewHolder, p1: Int) {
        p0.lbNamaList!!.text = BirthLIst[p1].nama

        Picasso.get().load("https://adminhimatifapps.researchdiploma.id/foto_mahasiswa/"+BirthLIst[p1].foto_mahasiswa).into(p0.lbImageList)


    }
}