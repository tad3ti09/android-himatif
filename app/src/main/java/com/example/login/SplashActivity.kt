package com.example.login

import android.content.SharedPreferences.Editor
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import com.example.login.Session.SharedPrefManager


class SplashActivity : AppCompatActivity() {
    lateinit var shared : SharedPrefManager
    private var tv1: View? = null
    var animation: Animation? = null
    private var editor: Editor? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        tv1 = findViewById(R.id.hello)
        animation = AnimationUtils.loadAnimation(applicationContext, R.anim.fade_in)
        tv1!!.animation = animation
        shared = SharedPrefManager(applicationContext)

        Handler().postDelayed({
            shared.checkLogin()
            finish()
        },5000)
    }
}


